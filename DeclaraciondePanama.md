# Declaración de Panamá sobre Ciencia Abierta

Integrantes de la academia y organizaciones de la sociedad civil de América Latina y el Caribe, reunidas en la Ciudad de Panamá en un evento paralelo al Foro CILAC 2018, entendemos que el conocimiento es un bien común. Queremos hacer pública nuestra confianza en el papel de la ciencia como motor de la democracia, la libertad y la justicia social en el actual momento histórico. Queremos más ciencia y la queremos abierta. Consideramos necesario transitar hacia modelos colaborativos de creación, gestión, comunicación, preservación y apropiación entre la Academia-Ciudadanía-Estado-Empresa. Por tanto, reconocemos que para abrir la ciencia se requiere ir más allá del acceso abierto, necesitamos recuperar el rol protagónico de la sociedad y reivindicar el derecho legítimo de los ciudadanos a producir y beneficiarse de la ciencia, la tecnología y la innovación. Para ello nos apoyamos en los siete principios contenidos en el “Manifiesto de ciencia abierta y colaborativa: hacia una ciencia abierta e inclusiva por el bienestar social y ambiental” promovido por la Red de Ciencia Abierta y Colaborativa para el Desarrollo (OCSDNet).

En el marco de los Objetivos de Desarrollo Sostenible, reconocemos la importancia de asegurar el acceso equitativo a educación de calidad y el desarrollo de conocimiento científico y de tecnologías socialmente útiles, para resolver los desafíos de desarrollo de nuestras naciones. Dichos desafíos requieren mayor participación social y fortalecer la cultura científica ciudadana, lo cual no resulta posible sin la apertura de la ciencia.

A nivel gubernamental, existe un creciente interés en desarrollar políticas de ciencia abierta como estrategia para mejorar la eficiencia y productividad de la inversión en ciencia y tecnología. Si bien esto es cierto, resulta fundamental que también se promueva la ciencia abierta como una herramienta clave para construcción de la ciudadanía y el  fortalecimiento de la democracia.

Para desplegar todo el potencial de la ciencia debemos avanzar en el diseño de políticas que consideren todos los elementos para la práctica de ciencia abierta con las particularidades de nuestra región y no tan solo adoptando tendencias internacionales sin ningún aporte crítico.

A través de esta declaración queremos contribuir a mejorar la comprensión de los beneficios de la ciencia abierta y plantear la necesidad de desarrollar políticas públicas integrales que atiendan los desafíos de la apertura a lo largo de todo el ciclo de investigación científica:

**Elementos esenciales para la práctica de ciencia abierta**   
![Elementos esenciales para la práctica de ciencia abierta
e](elementos_CA.png)  

## Estrategias de políticas públicas para la implementación de la ciencia abierta

Instamos a considerar e implementar las siguientes estrategias:

1. Formular políticas nacionales de ciencia abierta adaptadas a la problemática de América Latina y el Caribe. Promover la apertura en las políticas institucionales de Ciencia, Tecnología e Innovación e impulsar la adopción de políticas institucionales de ciencia abierta contemplando los elementos y estrategias indicadas en este documento.

2. Promover la publicación en acceso abierto en los términos de la Declaración de Berlín de todoslos productos de investigación en revistas de acceso abierto o repositorios interoperables en acceso abierto. Esto involucra fomentar el uso de licencias abiertas especialmente en aquellas investigaciones financiadas por subvenciones públicas y organismos de financiación nacionales o multilaterales.

3. Establecer incentivos económicos a investigaciones que presenten componentes de ciencia abierta, y promover financiación de procesos de innovación de código abierto en sectores productivos y de generación de política pública.

4. Establecer modelos que reconozcan que la colaboración entre actores científicos y las comunidades sociales como fundamental para la generación de conocimiento y el desarrollo de capacidades de la región. Deben incorporarse incentivos para la participación equitativa y significativa de las comunidades en procesos de investigación que se desarrollan desde la academia, así como la resolución de problemas comunitarios mediante procesos de ciencia ciudadana e innovación de código abierto de manera autónoma desde la sociedad civil.

5. Promover modelos de evaluación y medición de la producción científica que valoren la cooperación científica, la construcción y fortalecimiento de redes de conocimiento y los esfuerzos para desarrollar capacidades de investigación en la ciudadanía. Esto involucra la adopción de modelos de  producción científica que privilegien la publicación en acceso abierto y la construcción de indicadores de evaluación abierta aprovechando las capacidades instaladas en América Latina y el Caribe.

6. Incentivar el desarrollo de plataformas, infraestructuras y herramientas regionales para la ciencia abierta que sean abiertas e interoperables, y el fortalecimiento de repositorios actuales de documentos, datos y recursos educativos abiertos en las instituciones académicas y de investigación.

7. Programas de sensibilización y formación en ciencia abierta para investigadores, docentes universitarios de todo tipo, administradores de ciencia, administradores de educación superior y empresarios, así como introducir a estudiantes de pre y postgrado en las dinámicas de ciencia abierta. Instaurar prácticas pedagógicas orientadas a compartir conocimiento y a generar conocimiento de manera colaborativa.

8. Se considera prioritario el desarrollo de estrategias de implementación de ciencia ciudadana que fortalezcan procesos de apropiación social del conocimiento y promuevan la ciencia entre las dinámicas de la vida diaria; fomentando la generación y la reutilización de conocimiento científico desde las comunidades así como su participación activa en los procesos de construcción de agenda y prioridades de investigación.

9. Fomentar la inversión para la creación de instituciones de investigación abierta que cuenten con infraestructura física, tecnológica y humana para el desarrollo de proyectos científicos y de innovación con amplia participación ciudadana. Promover la creación de una red de laboratorios abiertos regionales que permita crear una agenda propia de proyectos colaborativos.

10. Formular políticas que se enfoquen en erradicar las desigualdades sociales en los procesos de generación de conocimiento y que cierren las brechas de participación por razones de raza, etnia, discapacidad o género y que impulse una ciencia que atienda la justicia social y también reconozca las asimetrías cognitiva entre los países.

11. Impulsar una red de ciencia abierta en la región que comparta recursos, datos e infraestructura para la investigación y la innovación con programas permanentes de libre intercambio de investigadores y estudiantes entre las distintas instituciones a través de convenios de cooperación multilateral.

Reconocemos que la implementación de la ciencia abierta supone un cambio cultural importante entre la comunidad científica y académica, además del empeño y compromiso de recursos de los administradores de ciencia nacionales e institucionales. Sin embargo, sabemos que los beneficios sociales que supone el desarrollo de la ciencia bien valen los esfuerzos que al respecto hagamos, por ello nuestras organizaciones tienen el propósito de aportar iniciativas y encontrar soluciones que ayuden a la pronta y óptima implementación de la ciencia abierta en América Latina y el Caribe.

Participantes en la construcción de la declaración,
