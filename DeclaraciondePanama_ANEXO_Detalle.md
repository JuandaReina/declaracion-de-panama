## ANEXO Declaración de Panamá sobre Ciencia Abierta: Detalle de los lineamientos para la construcción de políticas de ciencia abierta en América Latina y el Caribe - 2018

El reto de la investigación hoy se concentra en problemas globales de desigualdades estructurales, consignados alrededor de los Objetivos de Desarrollo Sostenible, que reúne los esfuerzos de distintas comunidades, entre ellas la científica, a fin de resolver desde principios cooperativos, multi y transdisciplinares aquellos problemas que aquejan al mundo.  Estos problemas en América Latina y el Caribe deben ser abordados desde una óptica regional que reconozca las potencialidades, las dificultades y las múltiples oportunidades de nuestros países. Entre las iniciativas que reconocemos como inspiradoras se encuentran: Proyecto Ciencia Abierta Ubatuba (Brasil, IBICT), Laboratorio de innovación para la paz (Colombia, Universidad Nacional de Colombia), Latindex-SciELO-Redalyc y CLACSO-LaReferencia (LATAM), BIOLEFT: semillas abiertas (Argentina, CENIT), SiB Colombia (Colombia, Instituto Humboldt), Coordinadora para la defensa del agua y de la vida (Cochabamba, Bolivia).

Queremos avanzar en la integración de nuevas prácticas de apertura de la ciencia,  mediante la incorporación de políticas que involucren todo el ciclo de investigación, incluyendo el diseño de  proyectos, recolección de datos, análisis y procesamiento, creación de infraestructura, publicación, divulgación y apropiación de los resultados. Buscamos contribuir al diseño de políticas que integren y reconozcan el papel activo de otros actores sociales en el proceso de producción de conocimiento científico, fomentando procesos inclusivos de inteligencia colectiva.

**Elementos esenciales para la práctica de ciencia abierta**   
![Elementos esenciales para la práctica de ciencia abierta
e](elementos_CA.png)  

Instamos a organismos gubernamentales, instituciones públicas y privadas, agencias de financiación de la ciencia e investigadores, a la sociedad civil y a la ciudadanía a promover las siguientes definiciones y prácticas de ciencia abierta para América Latina y el Caribe a partir de los siguientes ejes:

### Acceso Abierto

Comprende la publicación en acceso abierto contemplando la Ruta verde (vía repositorios) y Ruta dorada (vía revistas con política de acceso abierto sin cargos de procesamiento para lectores, ni para autores), para impresos, pre-impresos, materiales grises y publicaciones de cualquier tipo.

- Diseñar estrategias de mejoramiento de la calidad de revistas científicas y de divulgación en acceso abierto editadas en Latinoamérica y fomentar el desarrollo de competencias de los equipos editoriales.

- Diseñar estrategias de desarrollo y fortalecimiento de Repositorios digitales interoperables (nacionales, institucionales y/o temáticos), utilizando estándares de metadatos y directrices regionales vigentes.

- Fomentar el uso de recursos de información en acceso abierto y promoverlos en programas de alfabetización informacional.

### Datos Abiertos de investigación

Son datos que pueden ser utilizados, reutilizados y redistribuidos libremente por cualquier persona, y que se encuentran sujetos al requerimiento de atribución y de compartirse de la misma manera en que aparecen.

- Promover la publicación de los datos abiertos de investigación según las licencias y estándares disponibles asegurando la reproducción de los resultados de investigación.

- Asegurar la implementación y uso de mecanismos de datos abiertos en el gobierno para potenciar la transparencia, la rendición de cuentas y mejorar la toma de decisiones públicas.

- Impulsar la transparencia de algoritmos y otros mecanismo de recolección de datos desde el sector privado.

- Impulsar estrategias de soberanía de datos de la ciencia de la región.

### Educación Abierta

Entendida como prácticas y políticas que aseguren el acceso, apertura, modificación y reutilización de Recursos Educativos Abiertos (REAs), datos, metodologías y procesos educativos.

- Requiere entornos para la apropiación digital de la producción, acceso y el uso de los recursos educativos abiertos.

- Divulgación de recursos educativos abiertos (REAs) con el fin de integrarlos al sistema educativo a lo largo de la vida.

- Formar en capacidades para el desarrollo sostenible de materiales de aprendizaje de calidad abiertos.

- Preservación y conservación de REAs.

- Localizar las políticas sobre Educación Abierta (EA) valorando el nexo con otros movimientos de apertura.


### Ciencia ciudadana

Comprende la recolección, análisis y diseño de proyectos de investigación con diferentes actores de la sociedad civil atendiendo sus necesidades sociales y respetando la diversidad de experticias  no-científicas.

- Asegurar la transferencia social de productos de investigación, mediante agendas abiertas de investigación y la divulgación, apropiación y vulgarización de la ciencia

- Reconocer a la comunidad como sujeto participante del proceso de investigación y formar capacidades ciudadanas para la generación de ciencia desde las comunidades

- Estimular y reconocer los procesos científicos que se gestan desde las organizaciones de la sociedad civil y ámbitos comunitarios como actores de su propio desarrollo.

### Investigación abierta, reproducible y replicable

Comprende la apertura del ciclo de investigación científica o ciudadana mediante la utilización de instrumentos y herramientas abiertas, diseño de protocolos de investigación y notas de laboratorio, planes de gestión de datos, datos y metadatos entre diversos actores.

- Promover los flujos de trabajo colaborativo en abierto que promuevan la relevancia local, eficiencia y confiabilidad de la producción científica.

- Incluye el defender medidas como la protección frente a sesgos cognitivos, las metodologías y soportes metodológicos abiertos en la investigación y la transparencia en la documentación, notaciones científicas del trabajo y los resultados de investigación en abierto. (estudios reproducibles)

- Reconocer en la reproducibilidad un rol adicional en la enseñanza de los procesos de investigación, toda vez que los datos y el proceso de su análisis (código) liberados pueden ser usados como recursos educativos en la ciencia

### Evaluación Abierta

Entendido como los modelos de medición de la ciencia transparente y reproducible que valore la publicación científica, la generación de redes de conocimiento, la vinculación comunitaria y los impactos sociales en la solución de problemas regionales.

- Evaluación por pares de manera abierta que ayuden a evaluar buenas prácticas de las revistas, editores, autores y revisores en el ejercicio de validación, medición (Bibliometría y métricas alternativas de medición) y publicación de la información científica.

- Reconocer capacidades regionales para medir la visibilidad y posicionamiento de la producción científica.

- Reconocer el impacto en la resolución de problemas locales inmediatos como factor privilegiado de la medición científica.

- Modificar sistemas de evaluación académica para incentivar la publicación en revistas de acceso abierto.

### Herramientas Abiertas y Libres

Política de software y hardware libres

Comprende software e instrumental científico cuyo código, documentación de diseño y tutoriales se encuentran disponibles bajo licencias libres para su estudio, reutilización y modificación.

- Privilegiar al software libre para la investigación (generación, divulgación y uso de productos, construcción de redes de conocimiento identificación única de instituciones, investigadores, proyectos y productos de investigación).

- Liberar hardware y la totalidad del código para software es indispensable.

### Infraestructuras Abiertas

Comprende el diseño herramientas de gestión, exposición y preservación de datos que evitan la apropiación asimétrica de los resultados y aseguran la interoperabilidad, la transparencia y equitatividad de su uso.

- Asegurar la soberanía, sostenibilidad y preservación digital de repositorios a lo largo del tiempo, esto incluye repositorios de datos producidos por entidades públicas del Estado, repositorios de acceso abierto, repositorios de recursos educativos abiertos (REA).

- Disponer de infraestructuras digitales co-creadas abiertas en el territorio a fin de facilitar el acceso, preservación, apropiación y uso de las comunidades.

- Adopción de software libre para la gestión, producción y medición de la ciencia.

- Adopción de nodos nacionales de cosecha de repositorios para visibilidad y medición de impacto de productos de investigación de las naciones.

- Laboratorios abiertos.

### Innovación de Código Abierto

Comprende procesos de creación o co-creación de productos de conocimiento, entre diversos actores sociales (científicos, gubernamentales, ciudadanos y privados), los cuales se comprometen a trabajar con licencias abiertas, facilitando su uso y comercialización, pero asegurando su condición de apertura y  reutilización.

- Impulsar y reconocer la innovación, integrada por diversos actores sociales (científicos, gubernamentales, ciudadanos y privados) como medio de resolución de problemas sociales. Utilizando metodologías de la ciencia abierta y, por tanto, reconociendo al ciudadano como actor protagonista de su propio desarrollo.

- Promover el desarrollo de agendas compartidas para la investigación que mejore la calidad de vida de los habitantes de la región.

- Generar grupos de desarrollo colaborativo de la innovación basado en la incorporación de elementos de la ciencia abierta.

- Publicar en acceso abierto los resultados de procesos de innovación.

### Licencias libres

Entendidas como aquellos ejercicios de derechos de autor que reconociendo a los creadores, permiten la copia, modificación y la redistribución de las obras derivadas sin restricciones, salvo las que resguarden su libre circulación.

- Promover el uso de licencias que aseguren la apertura de productos de investigación en cualquiera de sus variedades y eviten el cierre futuro de las obras derivadas.

- Incentivar la publicación de las fuentes editables de las producciones, para fomentar su estudio, reutilización y replicabilidad.

- Asegurar la sostenibilidad de los proyectos, incentivando la reutilización del conocimiento y mejorando la sinergia entre los distintos actores.
