# Declaración de Panamá sobre Ciencia Abierta

Este documento estará abierto para comentarios hasta el 15 de diciembre de 2018. Vea las instrucciones abajo.

Más de 30 académicos, activistas y practicantes de ciencia abierta de la región respondieron a la invitación abierta por [Fundación Karisma](https://karisma.org.co/), [Centro de Internet y Sociedad de la Universidad del Rosario (ISUR)](http://isur.co/) y el [Sistema de Información para la Biodiversidad (SiB Colombia)](https://sibcolombia.net/) para iniciar un diálogo sobre cómo queremos que sean las políticas de ciencia abierta de América Latina.

Un trabajo previo y un encuentro presencial el 22 de octubre en la ciudad de Panamá, que tuvo lugar como [evento paralelo](https://karisma.org.co/disenar-las-politicas-de-ciencias-abierta-en-america-latina/) al Foro Abierto de Ciencias de América Latina y el Caribe 2018, permitió a este grupo acordar una serie de puntos comunes para abrir el debate y se plasmó en un **Documento Vivo** que se tituló Declaración de Panamá de Ciencia Abierta.

Este documento vivo se publica para comentarios y sugerencias de otros actores de la región que quieran contribuir y apoyar este proceso hasta el 15 de diciembre de 2018 y se abrirá a adhesiones después del 10 de enero de 2019.

Conozca la Declaración de Panamá sobre Ciencia Abierta y ¡ayúdenos a construir esta posición!

[Enlace para comentar](https://gitlab.com/declaraciondepanama/declaracion-de-panama/commit/2a6cb1120832bd5ce255242227e47da529912717)